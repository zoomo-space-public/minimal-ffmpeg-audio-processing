CONTAINER_REGISTRY="registry.gitlab.com"
DOCKER_PROJECT_GROUP_ID="zoomo-space-public"
DOCKER_PROJECT_ID="minimal-ffmpeg-audio-processing"

DOCKER_IMAGE_TAG="${1:?Missing version string}"
IMAGE_PATH=${CONTAINER_REGISTRY}/${DOCKER_PROJECT_GROUP_ID}/${DOCKER_PROJECT_ID}

docker login ${CONTAINER_REGISTRY}

docker build -t ${IMAGE_PATH}:${DOCKER_IMAGE_TAG} .

docker push $IMAGE_PATH:$DOCKER_IMAGE_TAG

