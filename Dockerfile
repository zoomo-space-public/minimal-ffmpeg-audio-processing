# This Docker image use ffmpeg to convert mp4 (m4a) audio input to mp3 audio output

# Dockerfile References: https://docs.docker.com/engine/reference/builder/
# =========================================================================
# build stage
# =========================================================================
ARG ALPINE_VERSION=3.14.2
FROM alpine:${ALPINE_VERSION} AS builder

ENV FFMPEG_VERSION=4.4

RUN apk add --update build-base curl nasm tar bzip2 lame-dev && \
  DIR=$(mktemp -d) && cd ${DIR} && \
  curl -s http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.gz | tar zxvf - -C . && \
  cd ffmpeg-${FFMPEG_VERSION} && \
  ./configure \
  --disable-everything \
  --disable-network \
  --disable-autodetect \
  --enable-small \
  --enable-decoder=aac*,mp3 \
  --enable-demuxer=mov,mp3 \
  --enable-muxer=mp3 \
  --enable-protocol=file \
  --enable-encoder=libmp3lame \
  --enable-libmp3lame \
  --enable-filter=aresample,asetrate,atempo,highpass,lowpass && \
  make install && \
  make distclean && \
  rm -rf ${DIR} && \
  apk del build-base curl tar bzip2 x264 openssl nasm && rm -rf /var/cache/apk/*

# Copy everything needed to /ffmpeg
RUN mkdir -p /ffmpeg/usr/lib /ffmpeg/lib /ffmpeg/bin && \
  cp /usr/lib/libmp3lame.a /usr/lib/libmp3lame.so.0 /ffmpeg/usr/lib && \
  cp /lib/ld-musl-x86_64.so.1 /ffmpeg/lib && \
  cp /usr/local/bin/* /ffmpeg/bin

# =========================================================================
# final stage
# =========================================================================
FROM scratch

ENV PATH=/bin

COPY --from=builder /ffmpeg /

# Example usage: docker run --rm -v=`pwd`:/tmp ffmpeg:latest ffmpeg -y -i /tmp/input.mp4 -af 'asetrate=44100*1.2,aresample=44100,atempo=1/1.2,highpass=200,lowpass=3000' output.mp3
